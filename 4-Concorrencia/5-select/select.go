package main

import (
	"fmt"
	"time"
)

func main() {
	canal1, canal2 := make(chan string), make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			canal1 <- "Canal1"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			canal2 <- "Canal2"
		}
	}()

	for {
		select {
		case msgCan1 := <-canal1:
			fmt.Println(msgCan1)
		case msgCan2 := <-canal2:
			fmt.Println(msgCan2)
		}
	}

}
