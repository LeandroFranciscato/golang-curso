package main

import (
	"fmt"
	"time"
)

func main() {
	canal := multiplexar(escrever("ola1"), escrever("ola2"))
	for i := 0; i <= 10; i++ {
		msg := <-canal
		fmt.Println(msg)
	}
}

func multiplexar(canaisEnt ...<-chan string) <-chan string {
	canalSaida := make(chan string)
	go func() {
		for {
			for _, canal := range canaisEnt {
				msg := <-canal
				canalSaida <- msg
			}
		}
	}()
	return canalSaida
}

func escrever(txt string) <-chan string {
	canal := make(chan string)
	go func() {
		for {
			canal <- fmt.Sprintf("Valor recebido: %s", txt)
			time.Sleep(time.Second / 4)
		}
	}()
	return canal
}
