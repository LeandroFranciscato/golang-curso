package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var waitgroup sync.WaitGroup
	waitgroup.Add(3)

	go func() {
		escrever(1, "Task1")
		waitgroup.Done()
	}()

	go func() {
		escrever(1, "Task2")
		waitgroup.Done()

	}()

	go func() {
		escrever(1, "Task3")
		waitgroup.Done()
	}()

	waitgroup.Wait()
}

func escrever(num int, txt string) {
	for i := 0; i < 5; i++ {
		fmt.Println(txt, num)
		num += 1
		time.Sleep(time.Millisecond * 500)
	}
}
