package main

import "fmt"

func main() {
	canal := make(chan string, 2)

	canal <- "Aoba"
	canal <- "Aob2"

	msg := <-canal
	msg2 := <-canal
	fmt.Println(msg)
	fmt.Println(msg2)
}
