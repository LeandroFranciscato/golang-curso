package main

import (
	"fmt"
	"time"
)

func main() {
	canal := make(chan string)
	go escrever("Task1", canal)
	for mensagem := range canal {
		fmt.Println(mensagem)
	}
}

func escrever(txt string, canal chan string) {
	for i := 0; i < 5; i++ {
		canal <- txt
		time.Sleep(time.Millisecond * 300)
	}
	close(canal)
}
