package main

import (
	"fmt"
	"time"
)

func main() {
	go escrever(1, "Task1") //goroutine
	escrever(9, "Task2")
}

func escrever(num int, txt string) {
	for {
		fmt.Println(txt, num)
		num += 1
		time.Sleep(time.Millisecond * 500)
	}
}
