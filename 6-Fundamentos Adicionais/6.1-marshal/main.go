package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
)

type Cachorro struct {
	Nome  string `json:"nome"`
	Raca  string `json:"raca"`
	Idade uint   `json:"idade"`
}

func main() {
	// struct
	cachorro := Cachorro{"Rex", "Pintcher", 3}
	cachorroJSON, erro := json.Marshal(cachorro)
	if erro != nil {
		log.Fatal(erro)
	}
	fmt.Println(bytes.NewBuffer(cachorroJSON))

	// map
	cachorro2 := map[string]interface{}{
		"Nome":  "Toby",
		"Raca":  "Poodle",
		"Idade": 5,
	}
	cachorro2JSON, erro := json.Marshal(cachorro2)
	if erro != nil {
		log.Fatal(erro)
	}
	fmt.Println(bytes.NewBuffer(cachorro2JSON))

}
