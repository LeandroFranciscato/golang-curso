package main

import (
	"fmt"
	"sort"
)

type Person struct {
	Name string
	Age  int
}

type Family struct {
	People []Person
}

func main() {
	family := Family{
		[]Person{
			{
				Name: "Leandro",
				Age:  35,
			},
			{
				Name: "Buddy",
				Age:  1,
			},
			{
				Name: "Giovana",
				Age:  30,
			},
		},
	}

	fmt.Println(family)

	sort.Slice(family.People, func(i, j int) bool {
		return family.People[i].Age < family.People[j].Age
	})

	fmt.Println(family)
}
