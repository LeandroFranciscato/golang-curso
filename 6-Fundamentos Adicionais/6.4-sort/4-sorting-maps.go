package main

import (
	"fmt"
	"sort"
)

func main() {
	people := map[string]int{
		"Giovana": 30,
		"Leandro": 35,
		"Buddy":   1,
	}

	var keys []string
	for key := range people {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	for _, key := range keys {
		fmt.Println(key, people[key])
	}
}
