package main

import (
	"fmt"
	"sort"
)

func main() {
	ints := []int{10, 5, 11, 3, 2, 50}
	sort.Ints(ints)

	fmt.Println(ints)

	var intsReversed []int
	for i, _ := range ints {
		intsReversed = append(intsReversed, ints[len(ints)-1-i])
	}

	fmt.Println(intsReversed)
}
