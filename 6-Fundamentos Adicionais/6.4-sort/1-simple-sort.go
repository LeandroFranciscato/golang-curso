package main

import (
	"fmt"
	"sort"
)

func main() {
	slice := []string{"y", "a", "b", "a", "c", "z"}
	sort.Strings(slice)
	fmt.Println(slice)
}
