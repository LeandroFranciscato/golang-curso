package main

import (
	"fmt"
	"sort"
)

type Person struct {
	Name string
	Age  int
}

type Family struct {
	People []Person
}

func (f Family) Len() int           { return len(f.People) }
func (f Family) Less(i, j int) bool { return f.People[i].Age < f.People[j].Age }
func (f Family) Swap(i, j int)      { f.People[i].Age, f.People[j].Age = f.People[j].Age, f.People[i].Age }

func main() {
	family := Family{
		[]Person{
			{
				Name: "Leandro",
				Age:  35,
			},
			{
				Name: "Buddy",
				Age:  1,
			},
			{
				Name: "Giovana",
				Age:  30,
			},
		},
	}

	fmt.Println(family)

	sort.Sort(family)

	fmt.Println(family)
}
