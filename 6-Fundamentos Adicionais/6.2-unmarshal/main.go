package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type Cachorro struct {
	Nome  string `json:"nome"`
	Raca  string `json:"raca"`
	Idade uint   `json:"idade"`
}

func main() {
	// struct
	cachorroJSON := `{"nome":"Rex","raca":"Pintcher","idade":3}`
	var cachorro Cachorro
	if erro := json.Unmarshal([]byte(cachorroJSON), &cachorro); erro != nil {
		log.Fatal(erro)
	}
	fmt.Println(cachorro)

	// map
	cachorro2JSON := `{"nome":"Rex","raca":"Pintcher","idade":3}`
	cachorro2 := make(map[string]interface{})
	if erro := json.Unmarshal([]byte(cachorro2JSON), &cachorro2); erro != nil {
		log.Fatal(erro)
	}
	fmt.Println(cachorro2)

}
