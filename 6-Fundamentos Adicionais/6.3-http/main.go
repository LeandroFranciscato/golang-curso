package main

import (
	"html/template"
	"log"
	"net/http"
)

var templates *template.Template

type Usuario struct {
	Nome      string
	SobreNome string
}

func main() {

	templates = template.Must(template.ParseGlob("*.html"))

	http.HandleFunc("/home", func(res http.ResponseWriter, req *http.Request) {
		usuario := Usuario{"Leandro", "Franciscato"}
		templates.ExecuteTemplate(res, "home.html", usuario)
	})
	log.Fatal(http.ListenAndServe(":5000", nil))
}
