package main

import (
	"api/src/config"
	"api/src/router"
	"fmt"
	"net/http"
)

func main() {
	config.Carregar()

	router := router.Gerar()
	http.ListenAndServe(fmt.Sprintf(":%d", config.PortaAPI), router)

}
