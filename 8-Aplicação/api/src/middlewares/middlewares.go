package middlewares

import (
	"api/src/auth"
	"api/src/response"
	"log"
	"net/http"
)

func Logger(next http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		log.Printf("\n %s %s %s", r.Method, r.RequestURI, r.Host)
		next(rw, r)
	}
}

func Auth(next http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		log.Printf("\n Autenticando...")
		if erro := auth.ValidarToken(r); erro != nil {
			response.Erro(rw, http.StatusForbidden, erro)
			return
		}
		next(rw, r)
	}
}
