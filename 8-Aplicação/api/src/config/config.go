package config

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

var (
	StringConexaoBanco = ""
	PortaAPI           = 0
	SecretKey          []byte
)

// Carregar inicializa as variaveis de ambiente
func Carregar() {
	var erro error = godotenv.Load()
	if erro != nil {
		log.Fatal(erro)
	}

	PortaAPI, erro = strconv.Atoi(os.Getenv("API_PORT"))
	if erro != nil {
		log.Fatal(erro)
	}

	StringConexaoBanco = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?%s",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PWD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_OPTIONS"),
	)

	SecretKey = []byte(os.Getenv("SECRET_KEY"))
}
