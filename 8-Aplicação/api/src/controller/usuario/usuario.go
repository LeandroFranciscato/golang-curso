package usuario

import (
	"api/src/auth"
	"api/src/banco"
	"api/src/controller"
	"api/src/model"
	"api/src/repo"
	"api/src/response"
	"errors"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

func Criar(rw http.ResponseWriter, r *http.Request) {
	body, err := controller.GetBody(r)
	if err != nil {
		response.Erro(rw, http.StatusUnprocessableEntity, err)
		return
	}

	// Preparação da entidade
	var usuario model.Usuario
	err = usuario.Unmarshal(body)
	if err != nil {
		response.Erro(rw, http.StatusBadRequest, err)
		return
	}
	usuario.CriadoEm = time.Now()
	err = usuario.Preparar("cadastro")
	if err != nil {
		response.Erro(rw, http.StatusBadRequest, err)
		return
	}

	// Nick/Email já existente
	db, err := banco.Connect()
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}
	repo := repo.UsuarioRepo(db)
	nickExistente, err := repo.IsNickExistente(usuario)
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}
	if nickExistente {
		response.Erro(rw, http.StatusConflict, errors.New("Nick já existente"))
		return
	}

	emailExistente, err := repo.IsEmailExistente(usuario)
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}
	if emailExistente {
		response.Erro(rw, http.StatusConflict, errors.New("Email já existente"))
		return
	}

	// Persistência
	usuario.ID, err = controller.Criar(reflect.ValueOf(&usuario))
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}

	response.JSON(rw, http.StatusCreated, usuario)
}

func ListarTodos(rw http.ResponseWriter, r *http.Request) {
	//filtro := r.URL.Query().Get("fitroai") se for utilizado ?filtroai=asdsad na URL

	filtro := mux.Vars(r)["filtro"]
	db, err := banco.Connect()
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repo.UsuarioRepo(db)
	usuarios, err := repo.ListarTodosUsuarios("Nick asc", filtro)
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}

	response.JSON(rw, http.StatusFound, usuarios)
}

func ListaUm(rw http.ResponseWriter, r *http.Request) {
	parametros := mux.Vars(r)
	usuarioID, err := strconv.ParseUint(parametros["id"], 10, 64)
	if err != nil {
		response.Erro(rw, http.StatusBadRequest, err)
		return
	}

	db, err := banco.Connect()
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}

	repo := repo.UsuarioRepo(db)
	usuario, err := repo.ListaUmUsuario(usuarioID)
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}

	response.JSON(rw, http.StatusFound, usuario)

}

func Atualiza(rw http.ResponseWriter, r *http.Request) {
	ID, err := strconv.ParseUint((mux.Vars(r)["id"]), 10, 64)
	if err != nil {
		response.Erro(rw, http.StatusBadRequest, err)
		return
	}
	body, err := controller.GetBody(r)
	if err != nil {
		response.Erro(rw, http.StatusUnprocessableEntity, err)
		return
	}

	var usuario model.Usuario
	err = usuario.Unmarshal(body)
	if err != nil {
		response.Erro(rw, http.StatusUnprocessableEntity, err)
		return
	}

	err = usuario.Preparar("editar")
	if err != nil {
		response.Erro(rw, http.StatusUnprocessableEntity, err)
		return
	}

	usuarioIDToken, err := auth.ExtrairUsuarioID(r)
	if err != nil {
		response.Erro(rw, http.StatusUnauthorized, err)
		return
	}
	if usuarioIDToken != ID {
		response.Erro(rw, http.StatusUnauthorized, errors.New("Você não pode modificar outro Usuário!"))
		return
	}

	db, err := banco.Connect()
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repo.UsuarioRepo(db)
	usuarioAtual, err := repo.ListaUmUsuario(ID)
	if err != nil {
		response.Erro(rw, http.StatusBadRequest, err)
		return
	}

	if usuarioAtual.Nick != usuario.Nick {

		nickExistente, err := repo.IsNickExistente(usuario)
		if err != nil {
			response.Erro(rw, http.StatusInternalServerError, err)
			return
		}
		if nickExistente {
			response.Erro(rw, http.StatusConflict, errors.New("Nick já existente"))
			return
		}
	}

	if usuarioAtual.Email != usuario.Email {
		emailExistente, err := repo.IsEmailExistente(usuario)
		if err != nil {
			response.Erro(rw, http.StatusInternalServerError, err)
			return
		}
		if emailExistente {
			response.Erro(rw, http.StatusConflict, errors.New("Email já existente"))
			return
		}
	}

	err = repo.AtualizarUsuario(ID, usuario)
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}

	response.JSON(rw, http.StatusNoContent, nil)
}

func Deleta(rw http.ResponseWriter, r *http.Request) {
	parametros := mux.Vars(r)
	ID, err := strconv.ParseUint(parametros["id"], 10, 64)
	if err != nil {
		response.Erro(rw, http.StatusBadRequest, err)
		return
	}

	usuarioIDToken, err := auth.ExtrairUsuarioID(r)
	if err != nil {
		response.Erro(rw, http.StatusUnauthorized, err)
		return
	}
	if usuarioIDToken != ID {
		response.Erro(rw, http.StatusUnauthorized, errors.New("Você não pode deletar outro Usuário!"))
		return
	}

	db, err := banco.Connect()
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repo.UsuarioRepo(db)
	err = repo.DeleteUsuario(ID)
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}

	response.JSON(rw, http.StatusGone, nil)
}
