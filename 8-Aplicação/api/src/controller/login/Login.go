package login

import (
	"api/src/auth"
	"api/src/banco"
	"api/src/controller"
	"api/src/model"
	"api/src/repo"
	"api/src/response"
	"errors"
	"net/http"
)

func Login(rw http.ResponseWriter, r *http.Request) {
	body, err := controller.GetBody(r)
	if err != nil {
		response.Erro(rw, http.StatusUnprocessableEntity, err)
		return
	}

	var usuario model.Usuario
	usuario.Unmarshal(body)

	db, err := banco.Connect()
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	repo := repo.UsuarioRepo(db)
	usuarioBanco, login, err := repo.Login(usuario.Email, usuario.Senha)
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
		return
	}
	if !login {
		response.Erro(rw, http.StatusForbidden, errors.New("Usuário/Senha Inválido!"))
		return
	}

	token, err := auth.CriarToken(int64(usuarioBanco.ID))
	if err != nil {
		response.Erro(rw, http.StatusInternalServerError, err)
	}
	response.JSON(rw, http.StatusOK, token)
}
