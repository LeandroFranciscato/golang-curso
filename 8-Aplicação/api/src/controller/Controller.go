package controller

import (
	"api/src/banco"
	"api/src/repo"
	"io/ioutil"
	"net/http"
	"reflect"
)

func GetBody(r *http.Request) ([]byte, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func Criar(entidade reflect.Value) (uint, error) {
	db, err := banco.Connect()
	if err != nil {
		return 0, err
	}

	repo := repo.Repo(db)
	insertedID, err := repo.Criar(entidade)
	if err != nil {
		return 0, err
	}
	return uint(insertedID), nil
}
