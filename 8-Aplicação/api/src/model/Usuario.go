package model

import (
	"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/badoux/checkmail"
)

// Usuario representa um usuário utilizando a rede social
type Usuario struct {
	ID       uint      `json:"id,omitempty"`
	Nome     string    `json:"nome,omitempty" searchable:"true"`
	Nick     string    `json:"nick,omitempty" searchable:"true"`
	Email    string    `json:"email,omitempty" searchable:"true"`
	Senha    string    `json:"senha,omitempty"`
	CriadoEm time.Time `json:"criadoEm,omitempty"`
	Idade    uint      `json:"idade,omitempty"`
}

func (usuario *Usuario) Unmarshal(body []byte) error {
	if err := json.Unmarshal(body, &usuario); err != nil {
		return err
	}
	return nil
}

func (usuario Usuario) validar(etapa string) error {
	if usuario.Nome == "" ||
		usuario.Email == "" ||
		usuario.Idade == 0 ||
		usuario.Nick == "" ||
		(usuario.Senha == "" && etapa == "cadastro") {
		return errors.New("Nome, Email, Idade, Nick e Senha  devem ser informado")
	}
	err := checkmail.ValidateFormat(usuario.Email)
	if err != nil {
		return errors.New("Email no formato inválido")
	}
	return nil
}

func (usuario *Usuario) formatar() {
	usuario.Nome = strings.TrimSpace(usuario.Nome)
	usuario.Nick = strings.TrimSpace(usuario.Nick)
	usuario.Email = strings.TrimSpace(usuario.Email)
	usuario.Senha = fmt.Sprintf("%x", md5.Sum([]byte(usuario.Senha)))
}

func (usuario *Usuario) Preparar(etapa string) error {
	err := usuario.validar(etapa)
	if err != nil {
		return err
	}
	usuario.formatar()
	return nil
}
