package rotas

import (
	"net/http"

	"api/src/middlewares"

	"github.com/gorilla/mux"
)

// Rota - todas as rotas HTTP da API
type Rota struct {
	URI        string
	Metodo     string
	Funcao     func(http.ResponseWriter, *http.Request)
	RequerAuth bool
}

var Rotas []Rota

func init() {
	Rotas = append(Rotas, rotasUsuario...)
	Rotas = append(Rotas, rotasLogin...)
}

func Configurar(rotas *mux.Router) *mux.Router {
	for _, rota := range Rotas {

		if rota.RequerAuth {
			rotas.HandleFunc(rota.URI,
				middlewares.Logger(
					middlewares.Auth(rota.Funcao))).Methods(rota.Metodo)
		}

		rotas.HandleFunc(rota.URI, middlewares.Logger(rota.Funcao)).Methods(rota.Metodo)
	}
	return rotas
}
