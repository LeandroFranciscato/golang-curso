package rotas

import (
	"api/src/controller/usuario"
	"net/http"
)

var rotasUsuario = []Rota{
	{
		URI:        "/usuario",
		Metodo:     http.MethodPost,
		Funcao:     usuario.Criar,
		RequerAuth: false,
	},
	{
		URI:        "/usuarios/{filtro}",
		Metodo:     http.MethodGet,
		Funcao:     usuario.ListarTodos,
		RequerAuth: true,
	},
	{
		URI:        "/usuario/{id}",
		Metodo:     http.MethodGet,
		Funcao:     usuario.ListaUm,
		RequerAuth: true,
	},
	{
		URI:        "/usuario/{id}",
		Metodo:     http.MethodPut,
		Funcao:     usuario.Atualiza,
		RequerAuth: true,
	},
	{
		URI:        "/usuario/{id}",
		Metodo:     http.MethodDelete,
		Funcao:     usuario.Deleta,
		RequerAuth: true,
	},
}
