package rotas

import (
	"api/src/controller/login"
	"net/http"
)

var rotasLogin = []Rota{
	{
		URI:        "/login",
		Metodo:     http.MethodPost,
		Funcao:     login.Login,
		RequerAuth: false,
	},
}
