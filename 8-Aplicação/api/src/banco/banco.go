package banco

import (
	"api/src/config"
	"database/sql"

	_ "github.com/lib/pq" // Driver postgress
)

// Conectar abre conexão com o banco de dados
func Connect() (*sql.DB, error) {
	db, erro := sql.Open("postgres", config.StringConexaoBanco)
	if erro != nil {
		return nil, erro
	}

	if erro = db.Ping(); erro != nil {
		db.Close()
		return nil, erro
	}

	return db, nil
}

func Query(stmt string, parameters []string) {

}
