package repo

import (
	"api/src/model"
	"crypto/md5"
	"database/sql"
	"fmt"
	"reflect"
)

func UsuarioRepo(db *sql.DB) *repo {
	return &repo{db}
}

func (repo repo) IsNickExistente(usuario model.Usuario) (bool, error) {
	db := repo.db

	statement, err := db.Prepare("select count(*) existe from usuario where nick = $1")
	if err != nil {
		return true, err
	}
	defer statement.Close()

	res, err := statement.Query(usuario.Nick)
	if err != nil {
		return true, err
	}

	var existe uint
	if res.Next() {
		err = res.Scan(&existe)
		if err != nil {
			return true, err
		}
	}
	return existe > 0, nil
}

func (repo repo) IsEmailExistente(usuario model.Usuario) (bool, error) {
	db := repo.db

	statement, err := db.Prepare("select count(*) existe from usuario where email = $1")
	if err != nil {
		return true, err
	}
	defer statement.Close()

	res, err := statement.Query(usuario.Email)
	if err != nil {
		return true, err
	}

	var existe uint
	if res.Next() {
		err = res.Scan(&existe)
		if err != nil {
			return true, err
		}
	}
	return existe > 0, nil
}

func (repo repo) ListarTodosUsuarios(orderField string, filtro string) ([]model.Usuario, error) {
	var usuario model.Usuario
	linhas, err := repo.listarTodos(reflect.ValueOf(&usuario), orderField, filtro)
	if err != nil {
		return nil, err
	}
	defer linhas.Close()

	var usuarios []model.Usuario
	for linhas.Next() {
		usuario = model.Usuario{}
		err = linhas.Scan(&usuario.ID, &usuario.Nome, &usuario.Nick, &usuario.Email, &usuario.Senha, &usuario.CriadoEm, &usuario.Idade)
		if err != nil {
			return nil, err
		}
		usuarios = append(usuarios, usuario)
	}

	return usuarios, nil
}

func (repo repo) ListaUmUsuario(ID uint64) (model.Usuario, error) {
	var usuario model.Usuario

	linha, err := repo.listaUm(reflect.ValueOf(&usuario), ID)
	if err != nil {
		return usuario, err
	}

	err = linha.Scan(&usuario.ID, &usuario.Nome, &usuario.Nick, &usuario.Email, &usuario.Senha, &usuario.CriadoEm, &usuario.Idade)
	if err != nil {
		return usuario, err
	}

	return usuario, nil
}

func (repo repo) AtualizarUsuario(ID uint64, usuario model.Usuario) error {
	defer repo.db.Close()
	stmt, err := repo.db.Prepare("update usuario set nome = $1, nick = $2, email = $3, idade = $4 where id = $5")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(usuario.Nome, usuario.Nick, usuario.Email, usuario.Idade, ID)
	if err != nil {
		return err
	}
	return nil
}

func (repo repo) DeleteUsuario(ID uint64) error {
	stmt, err := repo.db.Prepare("delete from usuario where ID = $1")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(ID)
	if err != nil {
		return err
	}

	return nil
}

func (repo repo) Login(email string, senha string) (model.Usuario, bool, error) {
	db := repo.db

	stmt, err := db.Prepare("select * from usuario where email = $1 and senha = $2")
	if err != nil {
		return model.Usuario{}, false, err
	}
	defer stmt.Close()

	var usuario model.Usuario
	senhaMD5 := fmt.Sprintf("%x", md5.Sum([]byte(senha)))

	linha, err := stmt.Query(email, senhaMD5)
	if linha.Next() {
		err = linha.Scan(&usuario.ID, &usuario.Nome, &usuario.Nick, &usuario.Email, &usuario.Senha, &usuario.CriadoEm, &usuario.Idade)
		if err != nil {
			return model.Usuario{}, false, err
		}
		return usuario, usuario.ID > 0, nil
	}
	return model.Usuario{}, false, nil

}
