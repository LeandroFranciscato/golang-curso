package repo

import (
	"database/sql"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"
)

type repo struct {
	db *sql.DB
}

func Repo(db *sql.DB) *repo {
	return &repo{db}
}

func (repo repo) fieldsAndValues(entidade reflect.Value) map[string]interface{} {

	fieldsAndValues := make(map[string]interface{})

	val := entidade.Elem()
	for i := 0; i < val.NumField(); i++ {

		var value string
		var tipo string = fmt.Sprintf("%s", val.Field(i).Type())

		if tipo == "time.Time" {
			timeField := val.Field(i)
			params := []reflect.Value{reflect.ValueOf("2006-01-02 15:04:05")}
			res := timeField.MethodByName("Format").Call(params)
			value = fmt.Sprintf("%s", res[0])
		} else {
			value = fmt.Sprintf("%v", val.Field(i).Interface())
		}

		field := val.Type().Field(i).Name
		fieldsAndValues[field] = value
	}

	return fieldsAndValues
}

func (repo repo) structName(entidade reflect.Value) string {
	return entidade.Elem().Type().Name()
}

func (repo repo) Criar(entidade reflect.Value) (uint, error) {
	db := repo.db
	defer db.Close()

	stmt := "insert into " + repo.structName(entidade) + " ("
	var values []interface{}
	var stringValues string

	fieldsAndValues := repo.fieldsAndValues(entidade)
	delete(fieldsAndValues, "ID")
	index := 0
	for field, value := range fieldsAndValues {
		values = append(values, value)
		if index == 0 {
			stmt = stmt + field
			stringValues = stringValues + "$" + strconv.Itoa(index+1)
		} else {
			stmt = stmt + "," + field
			stringValues = stringValues + ", $" + strconv.Itoa(index+1)
		}
		index++
	}
	stmt = stmt + ") values (" + stringValues + ")"

	statment, err := db.Prepare(stmt)
	if err != nil {
		log.Fatal(err)
	}
	defer statment.Close()

	_, err = statment.Exec(values...)
	if err != nil {
		log.Fatal(err)
	}

	rows, err := db.Query("select max(id) id from " + repo.structName(entidade))
	if err != nil {
		log.Fatal(err)
	}

	var lastID uint
	if rows.Next() {
		err = rows.Scan(&lastID)
		if err != nil {
			log.Fatal(err)
		}
	}

	return lastID, nil
}

func (repo repo) searchableClause(entidade reflect.Value, filtro string) string {
	elem := entidade.Elem()

	var where string = " where ("
	index := 0
	for i := 0; i < elem.NumField(); i++ {
		typeField := elem.Type().Field(i)
		tag := typeField.Tag
		if tag.Get("searchable") == "true" {
			clause := "upper(" + elem.Type().Field(i).Name + ") like '%" + strings.ToUpper(filtro) + "%'"
			if index == 0 {
				where = where + clause
			} else {
				where = where + " or " + clause
			}
			index++
		}
	}
	return where + " ) "
}

func (repo repo) listarTodos(entidade reflect.Value, orderField string, filtro string) (*sql.Rows, error) {
	db := repo.db

	where := repo.searchableClause(entidade, filtro)
	query := "select * from " + repo.structName(entidade) + where + " order by " + orderField

	linhas, erro := db.Query(query)
	if erro != nil {
		return nil, erro
	}

	return linhas, nil
}

func (repo repo) listaUm(entidade reflect.Value, ID uint64) (*sql.Row, error) {
	db := repo.db

	query := "select * from " + repo.structName(entidade) + " where ID = $1"

	stmt, erro := db.Prepare(query)
	if erro != nil {
		return nil, erro
	}
	defer stmt.Close()

	linha := stmt.QueryRow(ID)

	return linha, nil
}
