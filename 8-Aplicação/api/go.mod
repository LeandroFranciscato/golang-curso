module api

go 1.16

require (
	github.com/badoux/checkmail v1.2.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/lib/pq v1.10.0 // indirect
)
