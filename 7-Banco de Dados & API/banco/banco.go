package banco

import (
	"database/sql"

	_ "github.com/lib/pq" // Driver postgress
)

const (
	DB_DSN = "postgres://devbook:devbook@127.0.0.1:5432/devbook?sslmode=disable"
)

// Conectar abre conexão com o banco de dados
func Conectar() (*sql.DB, error) {
	db, erro := sql.Open("postgres", DB_DSN)
	if erro != nil {
		return nil, erro
	}

	if erro = db.Ping(); erro != nil {
		return nil, erro
	}

	return db, nil
}
