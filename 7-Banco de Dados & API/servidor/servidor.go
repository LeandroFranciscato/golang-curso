package servidor

import (
	"crud/banco"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type usuario struct {
	ID    int    `json:"id"`
	Nome  string `json:"nome"`
	Email string `json:"email"`
	Senha string `json:"senha"`
}

// CriarUsuario insere um usuário no banco
func CriarUsuario(rw http.ResponseWriter, r *http.Request) {
	corpoReq, erro := ioutil.ReadAll(r.Body)
	if erro != nil {
		rw.Write([]byte("Falha ao ler o body"))
		return
	}
	var usuario usuario

	if erro = json.Unmarshal(corpoReq, &usuario); erro != nil {
		rw.Write([]byte("Erro ao converter usuário para struct"))
		return
	}

	db, err := banco.Conectar()
	if err != nil {
		rw.Write([]byte("Erro ao conectar ao banco de dados: " + err.Error()))
		return
	}
	defer db.Close()

	stmt, err := db.Prepare("insert into usuario (nome, email, senha) values ($1, $2, $3)")
	fmt.Println(err)
	if err != nil {
		rw.Write([]byte("Erro ao realizar prepare stmt: " + err.Error()))
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(usuario.Nome, usuario.Email, usuario.Senha)
	if err != nil {
		rw.Write([]byte("Erro ao realizar inserção: " + err.Error()))
		return
	}

	// Postgres NÃO tem res.LastInsertId()
	rows, err := db.Query("select max(id) id from usuario")
	if err != nil {
		rw.Write([]byte("Erro ao obter o ID: " + err.Error()))
		return
	}
	defer rows.Close()

	var id int
	for rows.Next() {
		err = rows.Scan(&id)
		if err != nil {
			rw.Write([]byte("Erro ao obter o ID (2): " + err.Error()))
			return
		}
	}

	rw.WriteHeader(http.StatusCreated)
	rw.Write([]byte(fmt.Sprintf("Usuário inserido com sucesso, ID %d: ", id)))
}

// BuscarUsuarios retorna todos os usuários do banco de dados
func BuscarUsuarios(rw http.ResponseWriter, r *http.Request) {

	db, err := banco.Conectar()
	if err != nil {
		rw.Write([]byte("Erro ao conectar ao banco de dados: " + err.Error()))
		return
	}

	rows, err := db.Query("select id, nome, email, senha from usuario")
	if err != nil {
		rw.Write([]byte("Erro ao buscar usuarios: " + err.Error()))
		return
	}
	defer rows.Close()

	var usuarios []usuario
	for rows.Next() {
		var usuario usuario
		err = rows.Scan(&usuario.ID, &usuario.Nome, &usuario.Email, &usuario.Senha)
		if err != nil {
			rw.Write([]byte("Erro ao buscar usuarios (2): " + err.Error()))
			return
		}
		usuarios = append(usuarios, usuario)
	}

	rw.WriteHeader(http.StatusOK)
	if err = json.NewEncoder(rw).Encode(usuarios); err != nil {
		rw.Write([]byte("Erro ao converter usuarios para JSON: " + err.Error()))
		return
	}

}

// BuscarUsuarios retorna um usuário do banco de dados
func BuscarUsuario(rw http.ResponseWriter, r *http.Request) {
	parametros := mux.Vars(r)
	ID, err := strconv.ParseUint(parametros["id"], 10, 64)
	if err != nil {
		rw.Write([]byte("Erro ao converter parametro para inteito: " + err.Error()))
		return
	}

	db, err := banco.Conectar()
	if err != nil {
		rw.Write([]byte("Erro ao conectar ao banco: " + err.Error()))
		return
	}

	row, err := db.Query("select * from usuario where id = $1 ", ID)
	if err != nil {
		rw.Write([]byte("Erro ao buscar usuario: " + err.Error()))
		return
	}

	var usuario usuario
	if row.Next() {
		if err = row.Scan(&usuario.ID, &usuario.Nome, &usuario.Email, &usuario.Senha); err != nil {
			rw.Write([]byte("Erro ao buscar usuario (2): " + err.Error()))
			return
		}
	}

	if err := json.NewEncoder(rw).Encode(usuario); err != nil {
		rw.Write([]byte("Erro converter usuario para json: " + err.Error()))
		return
	}

}

// AtualizarUsuario altera os dados do usuário no banco
func AtualizarUsuario(rw http.ResponseWriter, r *http.Request) {
	par := mux.Vars(r)
	ID, err := strconv.ParseUint(par["id"], 10, 64)
	if err != nil {
		rw.Write([]byte("Erro converter parametro para inteiro: " + err.Error()))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		rw.Write([]byte("Erro ao obter o corpo da requisição: " + err.Error()))
		return
	}

	var usuario usuario
	if erro := json.Unmarshal(body, &usuario); erro != nil {
		if err != nil {
			rw.Write([]byte("Erro converter o json para struct: " + err.Error()))
			return
		}
	}

	db, err := banco.Conectar()
	if err != nil {
		rw.Write([]byte("Erro conectar o bd: " + err.Error()))
		return
	}
	defer db.Close()

	stmt, err := db.Prepare("update usuario set nome = $1, email = $2, senha = $3 where id = $4")
	if err != nil {
		rw.Write([]byte("Erro: " + err.Error()))
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(usuario.Nome, usuario.Email, usuario.Senha, ID)
	if err != nil {
		rw.Write([]byte("Erro: " + err.Error()))
		return
	}

	afetadas, err := res.RowsAffected()
	if err != nil {
		rw.Write([]byte("Erro: " + err.Error()))
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(fmt.Sprintf("Update realizado com sucesso, linhas afetadas %d: ", afetadas)))
}

//DeleteUsuario remove usuário do banco de dados
func DeleteUsuario(rw http.ResponseWriter, r *http.Request) {
	par := mux.Vars(r)
	ID, err := strconv.ParseUint(par["id"], 10, 64)
	if err != nil {
		rw.Write([]byte("Erro: " + err.Error()))
		return
	}

	db, err := banco.Conectar()
	if err != nil {
		rw.Write([]byte("Erro: " + err.Error()))
		return
	}
	defer db.Close()

	stmt, err := db.Prepare("delete from usuario where id = $1")
	if err != nil {
		rw.Write([]byte("Erro: " + err.Error()))
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(ID)
	if err != nil {
		rw.Write([]byte("Erro: " + err.Error()))
		return
	}

	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(fmt.Sprintf("Usuário %d deletado com sucesso", ID)))

}
