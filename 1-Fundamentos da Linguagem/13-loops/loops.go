package main

import (
	"fmt"
)

func main() {
	i := 0
	for i < 10 {
		i++
		fmt.Println("Incrementand i", i)
	}
	fmt.Println(i)

	for j := 0; j < 10; j++ {
		fmt.Println("Incrementando j", j)
	}

	// array ou slices
	nomes := [3]string{"joao", "dvid", "lucas"}
	for indice, valor := range nomes {
		fmt.Println(indice, valor)
	} // indice pode ser _ se não for desejado o uso

	// palavra
	for indice, letra := range "palavra" {
		fmt.Println(indice, string(letra))
		// cast ?
	}

	// map
	usuario := map[string]string{
		"nome":      "Leandro",
		"sobrenome": "Franciscato",
	}

	for chave, valor := range usuario {
		fmt.Println(chave, valor)
	}

	// slice de uma struct
	type pessoa struct {
		nome  string
		idade int
	}

	slicePessoa := []pessoa{
		{"leandro", 33},
		{"giovana", 29},
	}

	for indice, pessoa := range slicePessoa {
		fmt.Println(indice, pessoa)
	}

	// loop infinito
	for true {

	}

	// or

	for {

	}

}
