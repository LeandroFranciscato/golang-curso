package main

import "fmt"

func generica(i interface{}) {
	fmt.Println(i)
}

func main() {
	generica("string")
	generica(true)
	generica(1)
}
