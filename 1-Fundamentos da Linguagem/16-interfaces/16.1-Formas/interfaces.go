package main

import (
	"fmt"
	"math"
)

type Retangulo struct {
	altura  float64
	largura float64
}

func (r Retangulo) area() float64 {
	return r.altura * r.largura
}

type Circulo struct {
	raio float64
}

func (c Circulo) area() float64 {
	return math.Pi * math.Pow(c.raio, 2)
}

// se uma struct implementa um método da interface, tem que implementar todos, obrigatóriamente
type formaGeometrica interface {
	area() float64
	FazNada() string // tudo que implementa "area" deveria implementar FazNada, ou ocorrerá um erro no runtime
}

func calculaArea(f formaGeometrica) {
	fmt.Printf("A área da forma é %0.2f \n", f.area())
}

func main() {
	r := Retangulo{10, 15}
	calculaArea(r)

	c := Circulo{10}
	calculaArea(c)
}
