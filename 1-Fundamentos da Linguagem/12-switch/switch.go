package main

import "fmt"

func diaDaSemana(numero int) string {
	var dia string
	switch numero {
	case 1:
		dia = "Domingo"
		//fallthrough // retorna o próximo case independente da condição, meio inutil
	case 2:
		dia = "Segunda-Feira"
	case 3:
		dia = "Terça-Feira"
	case 4:
		dia = "Quarta-Feira"
		//break // não há necessidade
	case 5:
		dia = "Quinta-Feira"
	case 6:
		dia = "Sexta-Feira"
	case 7:
		dia = "Sabado"
	default:
		dia = "Dia Inválido"
	}
	return dia

}

func diaDaSemana2(numero int) string {
	switch {
	case numero == 1:
		return "Domingo"
	default:
		return "Não implementado ainda"
	}
}

func main() {
	dia := diaDaSemana(1)
	fmt.Println(dia)

	dia2 := diaDaSemana2(1)
	fmt.Println(dia2)
}
