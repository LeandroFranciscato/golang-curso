package main

import (
	"fmt"
)

type Usuario struct {
	nome     string
	idade    int8
	endereco Endereco
}

type Endereco struct {
	logradouro string
	numero     int
}

func main() {

	var usuario1 Usuario
	usuario1.nome = "Leandro"
	usuario1.idade = 33
	fmt.Println(usuario1)

	usuario2 := Usuario{"Leandro", 33, Endereco{"Rua tal", 200}}
	fmt.Println(usuario2)

	usuario3 := Usuario{nome: "Leandro"}
	usuario3.endereco.logradouro = "Logradouro teste"
	usuario3.endereco.numero = 400
	fmt.Println(usuario3)

}
