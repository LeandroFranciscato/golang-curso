package main

import (
	"fmt"
)

func main() {
	//ARRAY
	var array1 [5]int
	array1[0] = 12
	fmt.Println(array1)

	array2 := [5]int{1, 2, 3, 4, 5}
	fmt.Println(array2)

	array3 := [...]int{1, 2, 3, 4, 5, 6, 7}
	fmt.Println(array3)

	//SLICE
	slice1 := []int{1, 2, 3}
	fmt.Println(slice1)

	slice1 = append(slice1, 18)
	fmt.Println(slice1)

	// Slice pode ser um ponteiro para um array ou outro slice
	fmt.Println("-----------")

	slice2 := array3[1:4]
	fmt.Println(slice2)

	array3[1] = 5
	fmt.Println(slice2)

	slice3 := slice2[2:3]
	fmt.Println(slice3)

	slice2[2] = 55
	fmt.Println(slice3)

	// Arrays Internos
	fmt.Println("-----------")

	slice4 := make([]int, 10, 15)

	slice4 = append(slice4, 1)
	slice4 = append(slice4, 1)
	slice4 = append(slice4, 1)
	slice4 = append(slice4, 1)
	slice4 = append(slice4, 1)
	slice4 = append(slice4, 1) // Aqui a capacidade é dobrada, automaticamente
	slice4 = append(slice4, 1)

	fmt.Println(slice4)
	fmt.Println(len(slice4)) //tamanho
	fmt.Println(cap(slice4)) // capacidade

	slice5 := make([]string, 5)
	fmt.Println(slice5)

	fmt.Println(slice5)
	fmt.Println(len(slice5)) //tamanho
	fmt.Println(cap(slice5)) // capacidade

}
