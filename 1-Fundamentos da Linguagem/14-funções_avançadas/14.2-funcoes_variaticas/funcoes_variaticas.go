package main

import "fmt"

func soma(numeros ...int) int {
	total := 0
	for _, numero := range numeros {
		total += numero
	}

	return total
}

// Não posso ter mais de um parâmetro variático por função
// o variatico deve ser obrigatoriamente o ultimo parâmetro
func mostraNumero(texto string, numeros ...int) (string, int) {

	total := 0
	for _, numero := range numeros {
		total += numero
	}
	return texto, total
}

func main() {
	res := soma(1, 5, 8, 10, 50)
	fmt.Println(res)

	string1, numero1 := mostraNumero("EITA", 1, 2, 3, 4, 5, 6)
	fmt.Println(string1, numero1)

}
