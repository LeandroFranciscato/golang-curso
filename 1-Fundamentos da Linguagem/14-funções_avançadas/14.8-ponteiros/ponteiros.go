package main

import "fmt"

//passagem de parâmetro por referência
func inverteSinal(numero *int) {
	*numero = *numero * -1
}

func main() {
	n := 20
	inverteSinal(&n)
	fmt.Println(n)
}
