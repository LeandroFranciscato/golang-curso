package main

import "fmt"

func funcao1() {
	fmt.Println("Função 01")
}

func funcao2() {
	fmt.Println("Função 02")
}

// executa imadiatamente ao retorno
func alunaEstaAprovado(n1, n2 float64) bool {
	fmt.Println("Entrando na função")
	media := (n1 + n2) / 2

	defer fmt.Println("Média calculada, resultado será retornado")
	if media >= 6 {
		return true
	}
	return false
}

func main() {
	//defer funcao1()
	//funcao2()

	fmt.Println(alunaEstaAprovado(7, 8))

}
