package main

import "fmt"

var config int = 0

// posso ter uma por ARQUIVO
func init() {
	config = 10
	fmt.Println("Executando Init")
}

// posso ter uma por PACOTE
func main() {
	fmt.Println(config)
	fmt.Println("Função Main sendo executada")
}
