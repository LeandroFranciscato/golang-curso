package main

import (
	"fmt"
)

func main() {
	//ARITMETICOS
	soma := 1 + 2
	subtracao := 1 - 1
	divisao := 1 / 2
	multiplicacao := 1 * 5
	mod := 10 % 2

	fmt.Println(soma, subtracao, divisao, multiplicacao, mod)

	//ATRIBUIÇÃO
	fmt.Println("--------------")
	var var1 string = "a"
	var2 := "b"
	fmt.Println(var1, var2)

	//RELACIONAIS
	fmt.Println("--------------")
	fmt.Println(1 > 2)
	fmt.Println(1 < 2)
	fmt.Println(1 >= 2)
	fmt.Println(1 == 2)
	fmt.Println(1 != 2)

	//LÓGICOS
	fmt.Println("--------------")
	fmt.Println(true && false)
	fmt.Println(true || false)
	fmt.Println(!true)

	//OPERADORES UNÁRIOS
	fmt.Println("--------------")
	var numero int = 10

	numero++
	fmt.Println(numero)

	numero += 2
	fmt.Println(numero)

	numero--
	fmt.Println(numero)

	numero -= 5
	fmt.Println(numero)

	numero *= 2
	fmt.Println(numero)

	numero /= 2
	fmt.Println(numero)

	numero *= numero
	fmt.Println(numero)

	numero %= 10
	fmt.Println(numero)

	// OPERADOR TERNÁRIO NÃO EXISTE EM GO
	// TEM QUE USAR IF ELSE
	var texto string
	if numero > 5 {
		texto = "Maior que 5"
	} else {
		texto = "Menor que 5"
	}
	fmt.Println(texto)

}
