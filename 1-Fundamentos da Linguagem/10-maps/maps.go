package main

import "fmt"

func main() {
	usuario := map[string]string{
		"nome":      "Leandro",
		"sobrenome": "Franciscato",
	}

	fmt.Println(usuario)
	fmt.Println(usuario["nome"])

	usuario2 := map[string]map[string]string{
		"nome": {
			"primeiro": "Leandro",
			"ultimo":   "Franciscato",
		},
		"curso": {
			"nome":   "Engenharia",
			"campus": "Maringá",
		},
	}
	fmt.Println(usuario2)

	delete(usuario2, "curso")
	fmt.Println(usuario2)

	usuario2["signo"] = map[string]string{
		"nome": "virgem",
	}
	fmt.Println(usuario2)
}
