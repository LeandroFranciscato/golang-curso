package main

import "fmt"

func main() {
	numero := 0

	if numero > 15 {
		fmt.Println("Maior que 15")
	} else {
		fmt.Println("Menor ou igual a 15")
	}

	if outroNumero := numero; outroNumero > 0 {
		// outroNumero só existe no escopo do if/else
		fmt.Println("Número é maior que zero")
	} else if outroNumero <= 0 {
		fmt.Println("Menor/igualque zero")
	}

}
