package main

import (
	"fmt"
)

type Pessoa struct {
	nome      string
	sobrenome string
	idade     int8
	altura    float64
}

type Estudante struct {
	Pessoa
	curso     string
	faculdade string
}

func main() {
	var leandro Estudante = Estudante{
		Pessoa{
			"leandro",
			"franciscato",
			33,
			1.79},
		"Golang",
		"Udemy"}
	fmt.Println(leandro)
}
