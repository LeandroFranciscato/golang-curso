package main

import "fmt"

func main() {
	var var1 string = "Teste var 1"
	var2 := "Teste Var 2"
	fmt.Println(var1)
	fmt.Println(var2)

	// multipla declaração explicita
	var (
		var3 string = "var3"
		var4 string
	)
	fmt.Println(var3 + var4)

	// multipla declaração implicita
	var5, var6 := "valor var5", "valor var 6"
	fmt.Println(var5 + var6)

	//inversao valores
	var5, var6 = var6, var5
	fmt.Println("Invertido:" + var5 + var6)
}
