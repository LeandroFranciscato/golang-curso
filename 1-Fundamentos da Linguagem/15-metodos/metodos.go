package main

import (
	"fmt"
)

type Usuario struct {
	nome  string
	idade int
}

func (usuario Usuario) salvar() {
	fmt.Printf("Usuário %s Salvo \n", usuario.nome)
}

func (usuario Usuario) isMaiorIdade() bool {
	return usuario.idade >= 18
}

func (usuario *Usuario) aniversario(faz bool) {
	if faz {
		usuario.idade++
	}
}

func main() {
	var usuario1 Usuario = Usuario{"Leandro", 33}
	usuario1.salvar()
	bool := usuario1.isMaiorIdade()
	fmt.Println(bool)

	usuario1.aniversario(true)
	fmt.Println(usuario1)
}
