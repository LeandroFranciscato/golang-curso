package main

import (
	"errors"
	"fmt"
)

func main() {
	//int8, int16, int32, int64
	var int1 int16 = 100
	fmt.Println(int1)

	// utiliza arquitetura do computador por padrao
	// interencia de tipo, também segue arquitetura do computador
	var intPadrao int = 10
	fmt.Println(intPadrao)

	// nao pode ser negativa
	var int2 uint32 = 10
	fmt.Println(int2)

	// alias para int32, rune
	var int3 rune = 12345
	fmt.Println(int3)

	// alias para int8, byte
	var int4 byte = 123
	fmt.Println(int4)

	// floats
	// não existe apenas o float, a não ser implicito via inferencia de tipo
	var (
		float1 float32 = 10.5
		float2 float64 = 10.6
	)
	fmt.Println(float1)
	fmt.Println(float2)

	// sempre aspas duplas para strings
	var (
		caracter string = "texto"
	)
	fmt.Println(caracter)

	// char não existe em GO
	// aspas simples referencia a tabela ASC
	// ou seja um int
	char := 'h'
	fmt.Println(char)

	// valor zero, valor padrao por tipo primitivo
	// numero = 0
	// string = string vazia
	// erro = <nil>
	// bool = false
	var (
		texto int32
	)
	fmt.Println(texto)

	// boolean
	var mentira bool
	fmt.Println(mentira)

	// erro
	var erro error
	fmt.Println(erro)

	// criar um erro no gol
	var erroExistente error = errors.New("Um erro ai")
	fmt.Println(erroExistente)

}
