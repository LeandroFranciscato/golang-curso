package main

import (
	"errors"
	"fmt"
)

func somar(a int8, b int8) int8 {
	var intReturn int8 = a + b
	return intReturn
}

func calculosMatemaricos(num1, num2 float64) (float64, string, error) {
	var soma float64 = num1 + num2
	if soma > 10 {
		return 0, "", errors.New("Erro: Valor maior que 10")
	}
	return soma, "outro retorno", nil
}

func main() {
	soma := somar(10, 20)
	fmt.Println(soma)

	var f = func(txt string) string {
		return txt + " -> eita"
	}

	txt := f("Texto")
	fmt.Println(txt)

	//função com retorno de mais de um tipo
	resultadoSoma, resultadoString, _ := calculosMatemaricos(1.1, 1.2)
	fmt.Println(resultadoSoma)
	fmt.Println(resultadoString)

	// utiliza-se _ (underline) para ignorar um retorno, é tipo um /dev/null
	resultadoSoma2, _, erro := calculosMatemaricos(10.3, 10.4)
	if erro != nil {
		fmt.Println(erro)
	} else {
		fmt.Println(resultadoSoma2)
	}

	// chamada com retornos tipados
	var (
		floatRet  float64
		stringRet string
		erroRet   error
	)
	floatRet, stringRet, erroRet = calculosMatemaricos(0.1, 2)
	fmt.Println(floatRet)
	fmt.Println(stringRet)
	fmt.Println(erroRet)

}
