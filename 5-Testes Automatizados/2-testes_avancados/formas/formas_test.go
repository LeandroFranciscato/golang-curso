package formas_test

import (
	"math"
	. "testes/formas"
	"testing"
)

func TestArea(t *testing.T) {
	t.Run("Retangulo", func(t *testing.T) {
		retangulo := Retangulo{10, 12}
		areaEsperada := float64(120)
		areaRecebida := retangulo.Area()
		if areaEsperada != areaRecebida {
			t.Errorf("Área reebida %f diferente da esperada %f", areaRecebida, areaEsperada)
		}
	})

	t.Run("Circulo", func(t *testing.T) {
		circulo := Circulo{10}
		areaEsperada := float64(math.Pi * 100)
		areaRecebida := circulo.Area()
		if areaEsperada != areaRecebida {
			t.Errorf("Área reebida %f diferente da esperada %f", areaRecebida, areaEsperada)
		}
	})
}
