package formas

import (
	"fmt"
	"math"
)

type Retangulo struct {
	Altura  float64
	Largura float64
}

func (ret Retangulo) Area() float64 {
	return ret.Altura * ret.Largura
}

type Circulo struct {
	Raio float64
}

func (c Circulo) Area() float64 {
	return math.Pi * math.Pow(c.Raio, 2)
}

// se uma struct implementa um método da interface, tem que implementar todos, obrigatóriamente
type FormaGeometrica interface {
	Area() float64
}

func CalculaArea(f FormaGeometrica) {
	fmt.Printf("A área da forma é %0.2f \n", f.Area())
}
