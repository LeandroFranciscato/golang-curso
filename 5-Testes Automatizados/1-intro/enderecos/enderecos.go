package enderecos

import (
	"strings"
)

var TipoInvalido string = "Tipo Inválido"

// TiposValidos: retorna os tipos de endereço validos
func TiposValidos() []string {
	tiposValidos := []string{"Rua", "Avenida", "Estrada"}
	return tiposValidos
}

// TipoDeEndereco: retorna tipo de endereço, se válido
func TipoDeEndereco(enderecoCompleto string) string {

	tipo := strings.ToLower(strings.Split(enderecoCompleto, " ")[0])

	valido := false
	for _, tipoValido := range TiposValidos() {
		if strings.ToLower(tipoValido) == tipo {
			valido = true
			break
		}
	}
	if valido {
		return strings.Title(tipo)
	}
	return TipoInvalido
}
