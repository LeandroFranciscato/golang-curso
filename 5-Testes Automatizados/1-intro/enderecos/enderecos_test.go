package enderecos_test

import (
	. "testes/enderecos"
	"testing"
)

func TestTipoDeEndereco(t *testing.T) {
	t.Parallel()

	cenariosDeTeste := []string{
		"Rua abc",
		"Avenida abc",
		"Estrada abc",
		"Coiso abc",
		"Negócio",
	}

	for _, cenario := range cenariosDeTeste {
		enderecoTeste := cenario
		recebido := TipoDeEndereco(enderecoTeste)

		valido := false
		for _, tipo := range TiposValidos() {
			if tipo == recebido {
				valido = true
			}
		}
		if !valido && recebido != TipoInvalido {
			t.Error(recebido)
		}
	}

}
